#TAKEN From Axel cournac implementation of HiCspector

import scipy
import numpy
from scipy.sparse.linalg import eigsh

def get_Laplacian(M):
	S=M.sum(1)
	i_nz=numpy.where(S>0)[0]
	S=S[i_nz]
	M=(M[i_nz].T)[i_nz].T
	S=1/numpy.sqrt(S)
	M=S*M
	M=(S*M.T).T
	n=numpy.size(S)
	M=numpy.identity(n)-M
	M=(M+M.T)/2
	return M

def evec_distance(v1,v2):
	d1=numpy.dot(v1-v2,v1-v2)
	d2=numpy.dot(v1+v2,v1+v2)
	if d1<d2:
		d=d1
	else:
		d=d2
	return numpy.sqrt(d)

def get_ipr(evec):
	ipr=1.0/(evec*evec*evec*evec).sum()
	return ipr


def get_reproducibility(M1,M2,num_evec):
	k1=numpy.sign(M1).sum(1)
	d1=numpy.diag(M1)
	kd1=~((k1==1)*(d1>0))
	k2=numpy.sign(M2).sum(1)
	d2=numpy.diag(M2)
	kd2=~((k2==1)*(d2>0))
	iz=numpy.nonzero((k1+k2>0)*(kd1>0)*(kd2>0))[0]
	M1b=(M1[iz].T)[iz].T
	M2b=(M2[iz].T)[iz].T

	i_nz1=numpy.where(M1b.sum(1)>0)[0]
	i_nz2=numpy.where(M2b.sum(1)>0)[0]
	i_z1=numpy.where(M1b.sum(1)==0)[0]
	i_z2=numpy.where(M2b.sum(1)==0)[0]
	   
	M1b_L=get_Laplacian(M1b)
	M2b_L=get_Laplacian(M2b)
	   
	a1, b1=eigsh(M1b_L,k=num_evec,which="SM")
	a2, b2=eigsh(M2b_L,k=num_evec,which="SM")
	   
	b1_extend=numpy.zeros((numpy.size(M1b,0),num_evec))
	b2_extend=numpy.zeros((numpy.size(M2b,0),num_evec))
	for i in range(num_evec):
		b1_extend[i_nz1,i]=b1[:,i]
		b2_extend[i_nz2,i]=b2[:,i]
	   
	ipr_cut=5
	ipr1=numpy.zeros(num_evec)
	ipr2=numpy.zeros(num_evec)
	for i in range(num_evec):
		ipr1[i]=get_ipr(b1_extend[:,i])
		ipr2[i]=get_ipr(b2_extend[:,i])
	  
	b1_extend_eff=b1_extend[:,ipr1>ipr_cut]
	b2_extend_eff=b2_extend[:,ipr2>ipr_cut]
	num_evec_eff=min(numpy.size(b1_extend_eff,1),numpy.size(b2_extend_eff,1))
	  
	evd=numpy.zeros(num_evec_eff)
	for i in range(num_evec_eff):
		evd[i]=evec_distance(b1_extend_eff[:,i],b2_extend_eff[:,i])
	   
	Sd=evd.sum()
	l=numpy.sqrt(2)
	evs=abs(l-Sd/num_evec_eff)/l

	N=float(M1.shape[1]);
	if (numpy.sum(ipr1>N/100)<=1)|(numpy.sum(ipr2>N/100)<=1):
		print("at least one of the maps does not look like typical Hi-C maps")
	else:
		print("size of maps: %d" %(numpy.size(M1,0)))
		print("reproducibility score: %6.3f " %(evs))
		print("num_evec_eff: %d" %(num_evec_eff))
	return evs,num_evec_eff
