import h5py
import numpy as np



R=100000

fh5 = h5py.File('logratio-100kb-OE.hdf5', "r") #input in HDF5
mat=np.array(fh5['data'])
fh5.close()

print(np.shape(mat))

chrsize=[0,305, 197, 235, 186, 270] #chr size for each chr at 100kb
chrsizecum=[   0,  305,  502,  737,  923, 1193] #same put cumulate

outputname='logratio-circosformat-OE.txt' #output



def findchr(c,cumsumvec):
	z=0
	while c>cumsumvec[z+1]:
		z+=1
	if z==0:
		return z+1,c-cumsumvec[z]
	return z+1,c-cumsumvec[z]-1



#WRITE the conversion for circos read
fout=open(outputname,'w')
i=0
j=0
while i<np.shape(mat)[0]:
	chri,coordi=findchr(i,chrsizecum)
	print(i,coordi,chri)
	while j<np.shape(mat)[1]:
		chrj,coordj=findchr(j,chrsizecum)
		S=str(chri)+'\t'+str(coordi*R)+'\t'+str((coordi+1)*R)+'\t'
		S=S+str(chrj)+'\t'+str(coordj*R)+'\t'+str((coordj+1)*R)+'\t'+str(mat[i,j])+'\n'
		fout.write(S)
		j+=1
	i+=1
	j=i
fout.close()
