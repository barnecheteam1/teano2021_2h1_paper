#python 3
#2019-2021
#CC-By-SA
#Carron Leopold
#specific utils tools for hic need
#MOSTLY a copy from my other repository


import h5py
import numpy as np
from scipy import sparse
from hmmlearn import hmm
from copy import deepcopy

#DATA HELPER
###Common variable for every scripts
#
listchrhuman=["chr1","chr2","chr3","chr4","chr5","chr6","chr7","chr8","chr9","chr10","chr11","chr12","chr13","chr14","chr15","chr16","chr17","chr18","chr19","chr20","chr21","chr22","chrX"]
listchrmouse=["chr1","chr2","chr3","chr4","chr5","chr6","chr7","chr8","chr9","chr10","chr11","chr12","chr13","chr14","chr15","chr16","chr17","chr18","chr19"]
listchrdrosophila=["chr2L","chr2R","chr3L","chr3R",'chrX']
listchrchicken=["chr1","chr2","chr3","chr4","chr5","chr6","chr7","chr8","chr9","chr10","chr11","chr12","chr13","chr14","chr15","chr16","chr17","chr18","chr19","chr20"]#,"chr21","chr22","chr23","chr24","chr25","chr26","chr27","chr28","chr30","chr31","chr32","chr33","chrW","chrZ"]
listchrmaize=["1","2","3","4","5","7","8","9","10"]
listchrtair=["1","2","3","4","5"]


#all species that can be generated
dictchr={"Human":listchrhuman,
"Mouse":listchrmouse,
"Drosophila":listchrdrosophila,
"Chicken":listchrchicken,
"MAIZE":listchrmaize,
"TAIR":listchrtair}


#####
#ADDITIONAL LOADER

def LoadChrComplexCentromBeginandEnd(filenamein):
	"""
	in : a centromeric file from ucsc
	out : a dict d[achr]:pos_of_centrom
	USE IT for multiple centrom in you data	
	"""
	f=open(filenamein,'r')
	dout=dict()
	l=f.readline()
	while l:
		ls=l.split("\t")
		if ls[0] in dout:
			couple=[int(ls[1]),int(ls[2])] #[begin,end]
			dout[ls[0]].append(couple)
		else:
			dout[ls[0]]=list()
			couple=[int(ls[1]),int(ls[2])] #[begin,end]
			dout[ls[0]].append(couple)
		l=f.readline()
	f.close()
	return dout
	
def givematrixsize(sizedict,chrlist,achr,chrindex):
	"""
	input : chromosome_size_dict, chr list, achr, index of the chr in chrlist
	out : end,begin of matrix pos
	"""	
	Li=len(chrlist)
	#print(achr,chrindex,Li)
	if chrindex==(Li-1): 
		#print("fin de chromosome")
		return int(sizedict["HicUsedTotalSize"]),int(sizedict["HicChrBegin"+achr])
	else:
		return int(sizedict["HicChrBegin"+chrlist[chrindex+1]]),int(sizedict["HicChrBegin"+achr])
	

def loadchrsizedict(path,resolution):
	"""
	in : path to chromosomesize file from UCSC assembled genome
	out : a dict with size of each chr, containt both: TotalSize HicUsedTotalSize HicChrBegin
	"""
	f=open(path,"r")
	d={}
	l=f.readline()
	d["TotalSize"]=0
	d["HicUsedTotalSize"]=0
	while l:
		#print(l.strip('\n'))
		ls=l.split()
		d[ls[0]]=int(ls[1])
		d["TotalSize"]+=int(ls[1])
		F1=ls[0].__contains__("random")==False
		F2=ls[0].__contains__("Un")==False
		F3=str(ls[0])!="chrM"
		#F3=True
		F4=ls[0].__contains__("hap")==False
		#print(ls[0],ls[1],F1,F2,F3)
		if F1 & F2 & F3 & F4: #!!!! CARE !!!! All that part from the dict is generated for a specific resolution
			d[ls[0]]=np.float(ls[1])
			d["HicChrBegin"+ls[0]]=d["HicUsedTotalSize"]
			d["HicUsedTotalSize"]+=np.ceil(np.float(ls[1])/resolution)
		l=f.readline()
	f.close()
	return d

def loadabsdatafile(filein):
	"""
	in : a _abs.bedfile from HiCpro
	out : a dict with chr=[begin,end] in the bed, as finaly in the matrix
	"""
	fin=open(filein,"r")
	d={}
	d["Total"]=0
	l=fin.readline()
	while l:
		ls=l.split()
		if d.__contains__(ls[0]):
			d[ls[0]][1]=float(ls[3])
		else: #init
			d[ls[0]]=[float(ls[3]),float(ls[3])]
		d["Total"]+=1
		l=fin.readline()
	fin.close()
	return d


def extractfullmat(filein,SH):
	"""
	load the .matrix file from HICpro
	convert it into array for all chromosome in one step
	"""
	fin=open(filein,"r")
	outm=np.zeros((SH,SH))
	l=fin.readline()
	while l:
		ls=l.split()
		outm[int(ls[0])-1,int(ls[1])-1]=float(ls[2])
		outm[int(ls[1])-1,int(ls[0])-1]=float(ls[2])
		l=fin.readline()
	fin.close()
	return outm
	

def loadmatrixselected(filein,B,E):
	"""
	in : a .matrix file from HiCpro, is size
	out : the matrix generated
	i et j sont comprit entre B et E
	"""
	#E-=1
	B-=1 #file start at one
	sizemat=int(E-B)
	print("taille de la matrice:",sizemat)
	mat=np.zeros((sizemat,sizemat))
	fin=open(filein,"r")
	l=fin.readline()
	while l:
		ls=l.split()
		i=int(float(ls[0])-B)
		j=int(float(ls[1])-B)
		if i>=0 and j>=0 and i<sizemat and j<sizemat:
			#print(ls[0],ls[1],i,j,B,E)
			v=float(ls[2])
			mat[i,j]=v
			mat[j,i]=v
		l=fin.readline()
	fin.close()
	print("nombre de contact dans la matrice :",np.sum(np.triu(mat)))
	return mat


def loadhdf5(path):
	"""
	Give the filename in input, return the numpy array
	"""
	fh5 = h5py.File(path, "r")
	v=np.array(fh5['data'])
	fh5.close()
	return v

def writehdf5(path,v):
	"""
	output path, an object
	save the file in hdf5
	"""
	fh5 = h5py.File(path, "w")
	fh5['data'] = v
	fh5.close()
	return v

def writeasbed(vec,achr,resolution,fileout):
	"""
	INPUT
	numpy array to save (vec)
	str : the chr name
	int : resolution
	str : name of file to save
	"""
	out=open(fileout,'w')
	i=0
	L=vec.shape[0]
	while i<L:
		if vec[i]==-1:
			vec[i]=0
		s=achr+"\t"+str(i*resolution)+"\t"+str((i+1)*resolution)+"\t"+str(vec[i])
		s=str(s)+"\n"
		out.write(s)
		i+=1			
	out.close()
	
##### Binning tools
def bin2d(Data,p,q):    
    """   
	INPUT
    Data = input matrix
    p,q rescaling factors
    Written for sparse 
    OUTPUT
	Data with size divide by p and q
	"""
    n,m=np.shape(Data);
    s=(int(np.ceil(n/p)),int(np.ceil(m/q)))
    i,j,d = sparse.find(Data);
    i=np.int_(np.ceil(i/p))
    j=np.int_(np.ceil(j/q))
    M=sparse.csr_matrix((d,(i,j)))
    return M



def bin1D(anumpyarray,resolutionfrom,resolutionto):
	"""
	in : A numpy array , number of bin in raw and in col
	out : the matrix binned
	"""
	print(resolutionto,resolutionfrom)
	if resolutionto>resolutionfrom:
		convertionfactor=np.ceil(resolutionto/resolutionfrom)
		s=anumpyarray.shape
		newsizei=np.ceil(s[0]*resolutionfrom/resolutionto)
		newarray=np.zeros(int(newsizei))
		i=0
		while i<newsizei:
			ifrom=int(i*convertionfactor)
			ito=int((i+1)*convertionfactor)
			if i==newsizei-1:
				asum=np.sum(anumpyarray[ifrom:])
			else:
				asum=np.sum(anumpyarray[ifrom:ito])
			newarray[i]=asum
			i+=1
		return newarray
	elif resolutionto==resolutionfrom:
		print("no binning")
		return anumpyarray
	else:
		print("wrong resolution parameter in bin1D")

def bin2dfullmat(anumpyarray,resolutionfrom,resolutionto):
	"""
	in : A numpy array , number of bin in raw and in col
	out : the matrix binned
	Written for full
	"""
	print('change of resolution from ',resolutionfrom,' to ',resolutionto)
	if resolutionto>resolutionfrom:
		convertionfactor=np.ceil(resolutionto/resolutionfrom)
		s=anumpyarray.shape
		print("Initial HiC size before binning:",s)
		#has to be identical as result in other function like chrsizedict)
		newsizei=np.ceil(s[0]*resolutionfrom/resolutionto)
		newsizej=np.ceil(s[1]*resolutionfrom/resolutionto)
		newarray=np.zeros((int(newsizei),int(newsizej)))
		print("HiC size after binning :",newarray.shape)
		i=0
		j=0
		while i<newsizei:
			while j<newsizej:
				ifrom=int(i*convertionfactor)
				ito=int((i+1)*convertionfactor)
				jfrom=int(j*convertionfactor)
				jto=int((j+1)*convertionfactor)
				if i==newsizei-1:
					asum=np.sum(anumpyarray[ifrom:,jfrom:jto])
				elif j==newsizej-1:
					asum=np.sum(anumpyarray[ifrom:ito,jfrom:])
				elif i==newsizei-1 and j==newsizej-1:
					asum=np.sum(anumpyarray[ifrom:,jfrom:])
				else:
					asum=np.sum(anumpyarray[ifrom:ito,jfrom:jto])
				newarray[i,j]=asum
				newarray[j,i]=asum
				j+=1
			i+=1
			j=0
		return newarray
	elif resolutionto==resolutionfrom:
		print("No binning")
		return anumpyarray
	else:
		print("Wrong resolution parameter")

def reversesegmentation1D(segmentedvec,segmenter,originalsize):
	"""
	in : a matrix, a segmenter, his original size
	type of segmenter : list of coord wich has been saved
	out : the matrix that is reverse segmented
	/!\segmented equal to -1 (in matlab segmented val : 0)
	"""	
	reversedvec=np.zeros(originalsize)-1
	i=0
	Li=len(segmenter)
	while i<Li:
		reversedvec[segmenter[i]]=segmentedvec[i]
		i+=1
	return reversedvec
	




### ANALYSE
def HOMEMADEDLR(mat,cutoff=10):
	""" Home made implementation of HOMER DLR
	one windows in the midle (local) against 2 windows of the same size arround (distal)
	intput : the HiC matrix, the window size (cutoff)
	output : DLR (size of the matrix)
	"""
	L=mat.shape[0]
	i=0
	out=np.zeros(L)
	S2f=int(np.floor(cutoff/2))
	S2c=int(np.ceil(cutoff/2))
	#print(cutoff,S2f,S2c,2*S2f,2*S2c)
	while i<L:
		#print((i-S2f),(i+S2c))
		if (i-cutoff)<=0: #begin
			distal=np.sum(mat[i,cutoff:2*cutoff])
			local=np.sum(mat[i,:cutoff])
			#print('1',distal,local)
			out[i]=np.log2(distal/local)
		elif (i+cutoff)>=L: #end
			distal=np.sum(mat[i,(L-2*cutoff):(L-cutoff)])
			local=np.sum(mat[i,(L-cutoff):])
			#print('2',distal,local)
			out[i]=np.log2(distal/local)
		else : #easy case : in the middle of the sliding
			b=(i-S2f)
			e=(i+S2c)
			distal=np.sum(mat[i,(b-cutoff):b])+np.sum(mat[i,e:(e+cutoff)])
			local=np.sum(mat[i,b:e])
			s1=np.shape(mat[i,:b])
			s1b=np.shape(mat[i,e:])
			s2=np.shape(mat[i,b:e])
			#print('3',distal,local,s1,s1b,s2)
			out[i]=np.log2(distal/local)
		i+=1
	return out

def SCN(D, max_iter = 10):
	"""
	Out  : SCN(D)
	Code version from Vincent Matthys
	"""    
	# Iteration over max_iter    
	for i in range(max_iter):        
		D /= np.maximum(1, D.sum(axis = 0))       
		D /= np.maximum(1, D.sum(axis = 1))    
		# To make matrix symetric again   
	return (D + D.T)/2 

def observed_expected(OE):
	"""
	INPUT :
	SCN matrix
	OUPUT :
	OE matrix
	"""
	i=0
	j=0
	L=len(OE)
	while j<L:
		thediag=np.diag(OE,k=j)
		mtg=np.mean(thediag)
		while i<(L-j):
			v=OE[i,i+j]/mtg
			OE[i,i+j]=v
			OE[i+j,i]=v
			i+=1
		i=0
		j+=1
	return OE


def fastFloyd(contact):
	"""
	out : FF(contact)
	Code version from Vincent Matthys
	"""      
	n = contact.shape[0]    
	shortest = contact    
	for k in range(n):        
		i2k = np.tile(shortest[k,:], (n, 1))        
		k2j = np.tile(shortest[:, k], (n, 1)).T        
		shortest = np.minimum(shortest, i2k + k2j)    
	return shortest

def filteramat(Hicmat,factor):
	"""
	in : a HiCmat without any transformation, factor of reduction
	out : the HiCmatreduce,thevector of his transformation
	"""
	Hicmatreduce=deepcopy(Hicmat)
	#first step : filter empty bin
	sumHicmat=np.sum(Hicmat,0)
	segmenter1=np.where(sumHicmat>0)[0]
	Hicmatreduce=Hicmatreduce[np.argwhere(sumHicmat>0),np.where(sumHicmat>0)]
	#second step : filter lower bin
	sumHicmat=np.sum(Hicmatreduce,0)
	msum=np.mean(sumHicmat)
	mstd=np.std(sumHicmat)
	mini = msum-mstd*factor
	maxi = msum+mstd*factor
	#like mini < summat & summat <maxi in matlab
	newcond=mini < sumHicmat #CARE HERE : if you filter min and maxcond
	#newcond2=sumHicmat < maxi
	#newcond=np.logical_and(newcond1,newcond2)
	Hicmatreduce=Hicmatreduce[np.argwhere(newcond),np.where(newcond)]
	return Hicmatreduce,segmenter1[newcond]
	
def downsample_basic(contact, k):
	"""
	input : hicmatrix (contact), factor of downsampling
	output : value
	"""
	if k==100:
		return contact
	else :
		B = np.zeros(contact.shape)
		i, j = 0, 0
		L = contact.shape[0]
		while i < L:
			while j < L:
				if contact[i, j] != 0:
					B[i, j] = np.random.binomial(contact[i, j], k*1.0/100)
					B[j,i]=B[i, j]
				j += 1
			i += 1
			j = i
		return B

