import HiCtoolbox as HiCutils
import runrepro

import numpy
import os.path
import sys
import numpy as np
from numpy import linalg as LA
from sklearn.cluster import AgglomerativeClustering
import warnings


warnings.filterwarnings("ignore")

if sys.argv[1]=="Load-achr-in-Hic-pro-into-hdf5":
	print(sys.argv)
	d=loadabsdatafile(sys.argv[2]) #abs file
	print(d)
	chrlist=HiCutils.dictchr[sys.argv[4]] #species
	for i in chrlist:
		print(i)
		B=d[i][0]
		E=d[i][1]
		mat=loadmatrixselected(sys.argv[3],B,E) #.matrix
		print(sys.argv[5]+i+".hdf5") #repository out
		fh5 = h5py.File(sys.argv[5]+i+".hdf5", "w")
		fh5['data'] = mat
		fh5.close()
elif sys.argv[1]="All-hicpro-chr-into-hdf5":
	print(sys.argv)
	mat=extractfullmat(sys.argv[2],11916)#11916 size of thaliana genome at 10kb bin
	fh5 = h5py.File(sys.argv[3]+"full.hdf5", "w")
	fh5['data'] = mat
	fh5.close()
elif sys.argv[1]=="Spector-downsampling-effect":
	baseresolution=1000
	resolutionlist=[1000,2000,4000,10000,20000,40000,60000,80000,100000,150000]
	NBrep=30 #number of time you want to reproduce the downsampling
	outsim=np.zeros((NBrep,len(resolutionlist))) #Number of replicate, resolution
	HiCrep=sys.argv[2] #INPUT hdf5 data at 1kb resolution
	outrepository=sys.argv[3]
	mat=utils.loadhdf5(HiCrep)
	mat+=utils.loadhdf5(HiCrep)
	i=0
	while i<NBrep:
		matdownsample=HiCutils.downsample_basic(mat, 10)
		print(i,np.sum(np.sum(mat)),np.sum(np.sum(matdownsample)))
		Kj=0
		for resi in resolutionlist:
			print("=>",resi)
			matR=HiCutils.binamatrixin2d(mat,baseresolution,resi)
			matdownsampleR=HiCutils.binamatrixin2d(matdownsample,baseresolution,resi)
			v1=runrepro.get_reproducibility(matR,matdownsampleR,10)[0] #Number of eigen in it
			outsim[i,Kj]=v1
			Kj+=1
		i+=1
	utils.writehdf5(outrepository+'spectorsimmat-1kb.hdf5',outsim)
	print(outsim)
elif sys.argv[1]=="Boost-HiC":
	base=sys.argv[2] #INPUT hdf5 data at resolution of your wiil
	alpha=0.21
	mat=utils.loadhdf5(base)
	print('SCN')
	normmat=HiCutils.SCN(mat)
	print('boost')
	newchrmat=HiCutils.SCN(np.power(HiCutils.fastFloyd(1/np.power(normmat.copy(),alpha)),-1/alpha))
	print('adjust')
	newchrmatadj=HiCutils.adjustPdS(normmat,newchrmat) #adjust the PdS
	utils.writehdf5(base.split('.hdf5')[0]+'-boost.hdf5',newchrmatadj)
elif sys.argv[1]=="DLR-on-all-chr":
	baserep=sys.argv[2] 
	outname=sys.argv[3]
	HiC=utils.loadhdf5(baserep+".hdf5")
	HiC=HiCutils.SCN(HiC.copy())
	outhomer=np.zeros((48,len(HiC)))
	i=2
	while i<50:
		outhomer2[i-2,]=HiCutils.HOMEMADEDLR(HiC,i)
		i+=1
	utils.writehdf5(outname+"-DLRmatrix.hdf5",outhomer)
