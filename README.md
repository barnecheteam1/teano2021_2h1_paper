# Teano2021_2h1_paper

All script for written for https://doi.org/10.1101/2020.11.28.402172 

This repository contain several series of SCRIPT independant one to another here to make each task one by one.

In you need to reproduce one specific operation, please refer to the good script and don't hesitate to ask me question, I will be happy to help to reproduce the result present here.

# PYTHON SCRIPT

This folder help to do 6 distinct operation.

-In **main.py** you fill find code line to precroced ALL data for OTHER script operation.

**Load-achr-in-Hic-pro-into-hdf5** : Load HiC pro input in order to convert it into hdf5 (my own format for all operation). Separate chromosome one by one.

python3 main.py Load-achr-in-Hic-pro-into-hdf5 HiCpro-abs-file HiCpro-matrix-file species FOLDEROUT

**All-hicpro-chr-into-hdf5** : Load HiC pro input in order to convert it into hdf5 (my own format for all operation) all chromosome merge.

python3 main.py All-hicpro-chr-into-hdf5 HiCpro-matrix-file-at-10kb FOLDEROUT

**Spector-downsampling-effect** : Make the downsampling robustness test for a specific hdf5 file that start at 1kb resolution

python3 main.py Spector-downsampling-effect HiCdata-in-hdf5 FOLDEROUT


**Boost-HiC** : use boost-hic pipeline on a specific hdf5 with alpha = 0.2 as https://github.com/LeopoldC/Boost-HiC

python3 main.py Boost-HiC HiCdata-in-hdf5

Return the same hdf5 matrix but increased with boost-hic pipeline.

**DLR-on-all-chr** : generate home made DLR for size of 2 to 48 at desired resolution.

python3 main.py DLR-on-all-chr HiCdata-in-hdf5 repository-out

**Convert-for-circos** : to run circos script HiC need to be convert in Homer format, take full observed under expeted HiC matrix at 100kb and convert it on text as in HOMER format.

# MATLAB script 

Logratioonfull.m  :

Plot 2h1 under WT log ratio for desired dataset.

ITRzoom.m : 
Do specific view on ITR

Logratio.m : Plot 2h1 under WT log ratio with DLR and H3K27me3 mark

