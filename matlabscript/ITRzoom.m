%% ITR pretty

matAr='/2h1HiC/full-boost-adj-10kb.hdf5';
matA=h5read(matAr,'/data');
matA=SCN_sumV2(matA);

matBr='/WTHiC/full-boost-adj-10kb.hdf5';
matB=h5read(matBr,'/data');
matB=SCN_sumV2(matB);

chrsizecum=[1 3043  5013  7359  9218 11916];
resolution=10000;
aroundbound=1000000;
%ITR-Chr1R Chr1:
bI1=cast((15086191-aroundbound)/resolution,'uint64')
bE1=cast((15441067+aroundbound)/resolution,'uint64')  
%ITR-Chr4R Chr4:
bI2=cast((3192760-aroundbound)/resolution,'uint64')+chrsizecum(4)
bE2=cast((3265098+aroundbound)/resolution,'uint64')+chrsizecum(4)

E1z=bE1-bI1;
E2z=bE2-bI2;
mergesize=E1z+E2z;

matAzoom=zeros(mergesize);
matAzoom(1:E1z+1,1:E1z+1)=matA(bI1:bE1,bI1:bE1);
matAzoom(E1z:end,E1z:end)=matA(bI2:bE2,bI2:bE2);
matAzoom(1:E1z+1,E1z:end)=matA(bI1:bE1,bI2:bE2);
matAzoom(E1z:end,1:E1z+1)=matA(bI2:bE2,bI1:bE1);

matBzoom=zeros(mergesize);
matBzoom(1:E1z+1,1:E1z+1)=matB(bI1:bE1,bI1:bE1);
matBzoom(E1z:end,E1z:end)=matB(bI2:bE2,bI2:bE2);
matBzoom(1:E1z+1,E1z:end)=matB(bI1:bE1,bI2:bE2);
matBzoom(E1z:end,1:E1z+1)=matB(bI2:bE2,bI1:bE1);


figure,imagesc(log10(matAzoom))
colormap(flipud(hot))
axis square
colorbar
title('2h1')
set(gca,'xtick',[])
set(gca,'ytick',[])
caxis([-5 0])


figure,imagesc(log10(matBzoom))
colormap(flipud(hot))
axis square
colorbar
title('WT')
set(gca,'xtick',[])
set(gca,'ytick',[])
caxis([-5 0])



testtriu=triu(log10(matAzoom))+tril(log10(matBzoom));
figure,imagesc(testtriu)
colormap(flipud(hot))
axis square
colorbar
title('2h1 upper appart, WT lower part')
set(gca,'xtick',[])
set(gca,'ytick',[])

A=log2(matAzoom);
B=log2(matBzoom);
C=A-B;
C(isinf(C))=0;
C(isnan(C))=0;
gcf=figure,imagesc(C);
color=brewermap(200,'*RdBu');
colormap(color)
title('2h1 - WT on ITR region')
axis square
colorbar
set(gca,'xtick',[])
set(gca,'ytick',[])
caxis([-4 4])
