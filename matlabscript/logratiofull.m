%% log ratio on full

r=linspace(1,1,21);
base=0:0.05:1
colormapdirtyred=[ r ;1:-0.05:0  ; 1:-0.05:0 ];
colormapdirtyblue=[ base ;base ;r ];
colormapdirtyblue=colormapdirtyblue(:,1:20)
color=[colormapdirtyblue colormapdirtyred]

alpha=0.2;

i=2;
j=1;
matAr='/2h1HiC/full-boost-adj-10kb.hdf5';
matA=h5read(matAr,'/data');
matA=SCN_sumV2(matA);
[CBOA,EA]=observed_expected(sparse(matA),length(matA));
A=log2(CBOA); 
%A=log2(matA);

matBr='/WTHiC/full-boost-adj-10kb.hdf5';
matB=h5read(matBr,'/data');
matB=SCN_sumV2(matB);
[CBOB,EB]=observed_expected(sparse(matB),length(matB));
B=log2(CBOB); 

C=A-B;
C(isinf(C))=0;
C(isnan(C))=0;

gcf=figure,imagesc(C);
colormap(color')
axis square
T=strcat('logratio.svg');
%title(T)
set(gca,'xtick',[])
set(gca,'ytick',[])
colorbar
caxis([-1 1])
set(gca, 'FontName', 'Liberation Sans')
set(gca,'FontSize',20)

saveas(gcf,T,'svg') 
