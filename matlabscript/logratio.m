

r=linspace(1,1,21);
base=0:0.05:1
colormapdirtyred=[ r ;1:-0.05:0  ; 1:-0.05:0 ];
colormapdirtyblue=[ base ;base ;r ];
colormapdirtyblue=colormapdirtyblue(:,1:20)
color=[colormapdirtyblue colormapdirtyred]
color=color';


datapath='/data/otherdata/10kb/';
Bh1dlrhomer2=h5read(strcat(dlrpath,'H1triplicate-DLRmatrix.hdf5'),'/data')'; %dlr for k=2 to k=150
Bwtdlrhomer2=h5read(strcat(dlrpath,'WTtriplicate-DLRmatrix.hdf5'),'/data')';


polycombratio=load(strcat(datapath,'/H3K27me3.log2_2h1_over_Wt.10kb.bedgraph'));

matAr='/data/HiC/tair/matrix/merge/H1triplicate/full-boost-adj-10kb.hdf5';
matA=h5read(matAr,'/data');
matA=SCN_sumV2(matA);
[CBOA,EA]=observed_expected(sparse(matA),length(matA));
A=log2(CBOA);    

matBr='/data/HiC/tair/matrix/merge/WTtriplicate/full-boost-adj-10kb.hdf5';
matB=h5read(matBr,'/data');
matB=SCN_sumV2(matB);
[CBOB,EB]=observed_expected(sparse(matB),length(matB));
B=log2(CBOB);

C2=A-B;
C2(isinf(C2))=0;
C2(isnan(C2))=0;

b=cast((1150-200)*10/10,'int32')
e=cast((1771+200)*10/10,'int32')
%b=1284
%e=1829
b=cast((7358+280-200)*10/10,'int32')
e=cast((7358+630+200)*10/10,'int32')





for i=24 %value of K in DLR matrix

outname=strcat('/media/carron/UBUNTU 18_01/2h1review/varitr/itr4-10kb',int2str(i+1),'.svg')

gcf=figure,
ax1=subplot(4,2,[1 2 3 4])
imagesc(C2(b:e,b:e));
colormap(ax1,color)                                                                                                                    
%axis square
set(gca,'xtick',[])                                                                                                                 
set(gca,'ytick',[])                                                                              
%colorbar                                                                                                                            
caxis(ax1,[-1 1])                                                                                                                       
set(gca, 'FontName', 'Liberation Sans')                                                                                             
set(gca,'FontSize',20)  

ax6=subplot(4,2,[5 6])
plot(Bh1dlrhomer2(i,b:e)-Bwtdlrhomer2(i,b:e));
set(gca,'xtick',[])
ylabel('HOMER2 dlr')

ax7=subplot(4,2,[7 8])
plot(polycombratio(b:e,4))
set(gca,'xtick',[])
ylabel('ratio of H3K27me3 of 2h1 over WT')

linkaxes([ax1 ax6],'x')

saveas(gcf,outname,'svg') 

end

